#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente que abre un socket a un servidor."""

import socket
import sys
import time
from xml.sax import make_parser
from xml.sax.handler import ContentHandler

METODOS = ['BYE', 'ACK', 'REGISTER', 'INVITE']


class SmallSMILHandler(ContentHandler):
    """Class SmallSmilHandler."""

    def __init__(self):
        """Crear un diccionario para extraer los atributos."""
        # self.lista = []
        self.current_tag = ''
        self.fichero = {'account': ['username', 'passwd'],
                        'uaserver': ['ip', 'puerto'],
                        'rtpaudio': ['puerto'],
                        'regproxy': ['ip', 'puerto'],
                        'log': [''],
                        'audio': ['']}
        self.diccionario = {}

    def startElement(self, name, attrs):
        """Crear el diccionario con el ua1.xml."""
        if name in self.fichero:
            self.current_tag = name
            for atributo in self.fichero[name]:
                self.diccionario[name+'.'+atributo] = attrs.get(atributo, '')
        # print(self.diccionario)

    def endElement(self, name):
        """Funcion ejecuta al final de una etiqueta."""
        self.current_tag = ''

    def characters(self, content):
        """Leyendo fuera etiquetas."""
        if self.current_tag == 'log' or self.current_tag == 'audio':
            self.diccionario[self.current_tag] = content

    def get_tags(self):
        """Devuelve el diccionario."""
        return self.diccionario


def segundos(horas, minutos, segundos):
    """Para calcular los segundos."""
    h = horas * 3600
    m = minutos * 60
    return h + m + segundos


def log(mensaje, log_path):
    """Abrir un fichero para escribir en el."""
    fichero = open(log_path, "a")
    fecha = "%Y-%m-%d "
    now = time.gmtime(time.time())
    s = int(time.strftime('%S', now))
    m = int(time.strftime('%M', now))
    h = int(time.strftime('%H', now))
    ss = segundos(h, m, s)
    fichero.write(time.strftime(fecha, now) + str(ss) + ' ')
    fichero.write(mensaje.replace('\r\n', ' ') + "\n")
    fichero.close()


if __name__ == "__main__":
    if len(sys.argv) != 4:
        sys.exit("Usage: python uaclient.py config method option")
    try:
        configuracion = sys.argv[1]
        metodo = sys.argv[2]
        opcion = sys.argv[3]
    except IndexError:
        sys.exit("Usage: python3 uaclient.py config method option")
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    try:
        parser.parse(open(configuracion))
    except IndexError:
        sys.exit("Config: Fichero no encontrado")
    dicc_configuracion = cHandler.get_tags()
    """Añadimos una excepcion para si la ip esta vacia."""
    if dicc_configuracion['regproxy.ip'] == '':
        proxy_ip = '127.0.0.1'
    else:
        proxy_ip = dicc_configuracion['regproxy.ip']
    """Debemos hacer lo mismo con la ip del server."""
    if dicc_configuracion['uaserver.ip'] == '':
        server_ip = '127.0.0.1'
    else:
        server_ip = dicc_configuracion['uaserver.ip']
    """Almacenamos los datos necesarios para establecer conexion."""
    puerto_server = dicc_configuracion["uaserver.puerto"]
    puerto_proxy = int(dicc_configuracion["regproxy.puerto"])
    username = dicc_configuracion["account.username"]
    password = dicc_configuracion["account.passwd"]
    puerto_rtp = dicc_configuracion["rtpaudio.puerto"]
    audio = dicc_configuracion["audio"]
    log_path = dicc_configuracion["log"]
    """Debemos escribir en el fichero log"""
    log("Starting...", log_path)

    """Creamos el socket."""
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            my_socket.connect((proxy_ip, int(puerto_proxy)))
            # log('Starting...', log_path)
            if metodo == 'REGISTER':
                mensaje = metodo + " sip:" + username + ':' \
                          + str(puerto_server) + " SIP/2.0\r\n"
                mensaje2 = "Expires: " + str(opcion) + "\r\n\r\n"
                LINE = mensaje + mensaje2
                print('Enviado:', LINE)
            elif metodo == 'INVITE':
                mensaje = metodo + " sip:" + opcion + " SIP/2.0\r\n"
                usuario = username.split(':')[0]
                typo = ('v=0\r\n'
                        + 'o=' + username + ' ' + server_ip + '\r\n'
                        + 's=misesion\r\n'
                        + 't=0\r\n'
                        + 'm=audio ' + puerto_rtp + ' RTP')
                content = 'Content-Type: application/sdp\r\nContent-Length: ' \
                          + str(len(bytes(typo, 'utf-8'))) + \
                          '\r\n\r\n'
                length_sdp = str(len(bytes(typo, 'utf-8')))
                # Falrta content-lenght y content-type
                LINE = mensaje + content + typo
                # print(LINE)
            elif metodo == 'BYE':
                LINE = metodo + " sip:" + opcion + " SIP/2.0\r\n"
            elif metodo not in METODOS:
                sys.exit('Usage: 405 Method not allowed')
            """Solo si se produce el metodo INVITE"""
            my_socket.send(bytes(LINE, 'utf-8'))
            log('Sent to ' + proxy_ip + ':' + str(puerto_proxy)
                + ': ' + LINE, log_path)
            data = my_socket.recv(1024)
            log('Received from ' + proxy_ip + ':' + str(puerto_proxy)
                + ': ' + data.decode('utf-8'), log_path)
            print('Recibido', data.decode('utf-8'))
            respuesta_servidor1 = data.decode('utf-8')
            if metodo == 'INVITE':
                # print(respuesta_servidor1)
                trying = respuesta_servidor1.split('\r\n\r\n')[0] \
                         == 'SIP/2.0 100 Trying'
                ringing = respuesta_servidor1.split('\r\n\r\n')[1]\
                    == 'SIP/2.0 180 Ringing'
                msg = respuesta_servidor1.split('\r\n\r\n')[2]
                ok = msg.split('\r\n')[0] \
                    == 'SIP/2.0 200 OK'
                mens_log = msg.split('\r\n')[0]
                if trying and ringing and ok:
                    LINE2 = ('ACK' + ' sip:' + opcion + " SIP/2.0")
                    print('Enviando ACK...: ' + LINE2)
                    my_socket.send(bytes(LINE2, ' utf-8') + b'\r\n')
                    log('Sent to ' + proxy_ip + ':' + str(puerto_proxy)
                        + ': ' + LINE2, log_path)
                    with socket.socket(socket.AF_INET,
                                       socket.SOCK_DGRAM) as my_socket_ack:
                        my_socket_ack.setsockopt(socket.SOL_SOCKET,
                                                 socket.SO_REUSEADDR, 1)
                        my_socket_ack.connect((server_ip, int(puerto_server)))
                        log('Sent to ' + server_ip + ':'
                            + str(puerto_server) + ': '
                            + data.decode('utf-8'), log_path)
                        my_socket_ack.send(data)

            log('Finishing.', log_path)
    except IndexError:
        sys.exit('Usage: 400 Bad Request')
