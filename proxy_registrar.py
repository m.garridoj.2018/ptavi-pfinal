#!/usr/bin/python3
"""Clase (y programa principal) para un servidor de eco en UDP simple."""
import socketserver
import sys
import json
import socket
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from uaclient import log
import time


fecha = "%Y-%m-%d %H:%M:%S"
tiempoahora = time.time()
tiempoluego = time.time()-tiempoahora
tiempoint = int(tiempoluego)
tiempo_min = tiempoint/60
tiempo_seg = tiempo_min/60


class SmallSMILHandler(ContentHandler):
    """Echo server class."""

    def __init__(self):
        """Crear diccionario extraer en el pr.xml."""
        self.current_tag = ''
        self.mensaje = {'server': ['name', 'ip', 'puerto'],
                        'database': ['path', 'passwdpath'],
                        'log': ['']}
        self.dicc = {}

    def startElement(self, name, attrs):
        """Crear diccionario con el pr.xml."""
        if name in self.mensaje:
            self.current_tag = name
            for atributo in self.mensaje[name]:
                if name != 'log':
                    self.dicc[name + '.' + atributo] = attrs.get(atributo, '')

    def endElement(self, name):
        """Funcion ejecuta al final de una etiqueta."""
        self.current_tag = ''

    def characters(self, content):
        """Leyendo fuera etiquetas."""
        if self.current_tag == 'log':
            print('ok')
            self.dicc[self.current_tag] = content

    def get_tags(self):
        """Devuelve diccionario."""
        return self.dicc


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    diccionario_json = {}

    def json2register(self):
        """Comprobar que el archivo no este creado."""
        try:
            with open(database_path, "r") as jsonfile:
                self.diccionario_json = json.load(jsonfile)
        except IndexError:
            """En caso de que no este creado continuamos para crearlo."""
            pass

    def register2json(self):
        """Abrir el fichero y escribir en el."""
        with open(database_path, "w") as jsonfile:
            json.dump(self.diccionario_json, jsonfile, indent=3)

    def handle(self):
        """handle method of the server class."""
        self.json2register()
        puerto = self.client_address[1]
        cliente = self.client_address[0]
        print("IP CLIENTE: " + cliente + "\n\r")
        print("PUERTO CLIENTE: " + str(puerto) + "\r\n")
        mensaje = self.rfile.read()
        cadena = mensaje.decode('utf-8')
        log('Received from ' + cliente + ':'
            + str(puerto) + ': ' + cadena, log_path)
        print(cadena)
        # nombre_usuario = {"address": "", "expires": ""}

        """Añadimos una lista para almacenar el mensaje."""

        cadena1 = cadena.split(' ')[0]
        if cadena1 == "REGISTER":
            print('register')
            usuario = cadena.split(' ')[1].split(':')[1]
            puerto = int(cadena.split(' ')[1].split(':')[2])
            ip_cliente = self.client_address[0]
            expira = int(cadena.split('\r\n')[1].split(' ')[1])
            horaactual = time.time()
            if expira != 0:
                self.diccionario_json[usuario] = \
                    [puerto, ip_cliente, expira, horaactual]
            else:
                if usuario in self.diccionario_json:
                    del self.diccionario_json[usuario]
            self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
            mensaje = 'SIP/2.0 200 OK\r\n\r\n'
            log('Sent to ' + cliente + ':'
                + str(self.client_address[1]) + ': ' + mensaje, log_path)
        elif cadena1 == 'INVITE':
            usuario_dest = cadena.split('\r\n')[0].split(' ')[1].split(':')[1]
            usuario_orig = cadena.split('\r\n')[5].split(' ')[0].split('=')[1]
            if usuario_dest in self.diccionario_json \
                    and usuario_orig in self.diccionario_json:
                ip_dest = self.diccionario_json[usuario_dest][1]
                puerto_dest = self.diccionario_json[usuario_dest][0]
                with socket.socket(socket.AF_INET,
                                   socket.SOCK_DGRAM) as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((ip_dest, puerto_dest))
                    my_socket.send(bytes(cadena, 'utf-8'))
                    log('Sent to ' + ip_dest + ': ' + str(puerto_dest) + ': '
                        + cadena, log_path)
                    data = my_socket.recv(1024)
                    log('Received from ' + ip_dest + ': ' + str(puerto_dest)
                        + ': ' + data.decode('utf-8'), log_path)
                    print(data.decode('utf-8'))
                self.wfile.write(data)
                log('Sent to ' + ip_dest + ': ' + str(puerto_dest) + ': '
                    + data.decode('utf-8'), log_path)
            else:
                self.wfile.write(b'SIP/2.0 404 User Not Found\r\n\r\n')
                log('Sent to ' + cliente + ': ' + str(self.client_address[1])
                    + ': SIP/2.0 404 User Not Found', log_path)
        elif cadena1 == 'BYE':
            print('bye')
            usuario_dest = cadena.split('\r\n')[0].split(' ')[1].split(':')[1]
            if usuario_dest in self.diccionario_json:
                ip_dest = self.diccionario_json[usuario_dest][1]
                puerto_dest = self.diccionario_json[usuario_dest][0]
                with socket.socket(socket.AF_INET,
                                   socket.SOCK_DGRAM) as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((ip_dest, puerto_dest))
                    my_socket.send(bytes(cadena, 'utf-8'))
                    log('Sent to ' + ip_dest + ': '
                        + str(puerto_dest) + ': ' + cadena, log_path)
                    data = my_socket.recv(1024)
                    log('Received from ' + ip_dest + ': '
                        + str(puerto_dest) + ': '
                        + data.decode('utf-8'), log_path)
                self.wfile.write(data)
                log('Sent to ' + ip_dest + ': ' + str(puerto_dest)
                    + ': ' + data.decode('utf-8'), log_path)
        elif cadena1 == 'ACK':
            usuario_dest = cadena.split('\r\n')[0].split(' ')[1].split(':')[1]
            if usuario_dest in self.diccionario_json:
                ip_dest = self.diccionario_json[usuario_dest][1]
                puerto_dest = self.diccionario_json[usuario_dest][0]
                with socket.socket(socket.AF_INET,
                                   socket.SOCK_DGRAM) as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((ip_dest, puerto_dest))
                    my_socket.send(bytes(cadena, 'utf-8'))
                    log('Sent to ' + ip_dest + ': '
                        + str(puerto_dest) + ': ' + cadena, log_path)
            # print('okey')
        else:
            self.wfile.write(b'SIP/2.0 405 Method Not Allowed\r\n\r\n')
            log('Sent to ' + cliente + ': ' + str(self.client_address[1])
                + ': SIP/2.0 405 Method Not Allowed\r\n\r\n', log_path)
        self.register2json()


if __name__ == "__main__":
    try:
        config = sys.argv[1]
    except IndexError:
        sys.exit("Usage: python3 uaserver.py config")
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    try:
        parser.parse(open(config))
    except FileNotFoundError:
        sys.exit("Usage: python3 uaserver.py config")
    diccionario_proxy = cHandler.get_tags()
    if diccionario_proxy['server.ip'] == '':
        ip = '127.0.0.1'
    else:
        ip = diccionario_proxy['server.ip']
    server_name = diccionario_proxy['server.name']
    database_path = diccionario_proxy['database.path']
    puerto_proxy = diccionario_proxy['database.passwdpath']
    print(diccionario_proxy)
    log_path = diccionario_proxy['log']
    puerto_escucha = diccionario_proxy['server.puerto']

    serv = socketserver.UDPServer((ip, int(puerto_escucha)),
                                  SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    log('Starting...', log_path)
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        log('Finishing.', log_path)
