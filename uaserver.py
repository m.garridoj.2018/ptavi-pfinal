#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import sys
import socketserver
from xml.sax import make_parser
from uaclient import SmallSMILHandler, log
import simplertp
import os
import random
import secrets


class EchoHandler(socketserver.DatagramRequestHandler):
    """Class EchoChandler."""

    list_rtp = []

    def handle(self):
        """Funcionamiento metodos."""
        IP_CLIENTE = str(self.client_address[0])
        print(IP_CLIENTE)
        PUERTO_CLIENTE = str(self.client_address[1])
        print(PUERTO_CLIENTE + '\r\n')
        metodos = ['INVITE', 'BYE', 'ACK']

        print("Hemos recibido tu peticion ")
        # while 1:
        # Leyendo línea a línea lo que nos envía el cliente
        LINE = self.rfile.read()
        # recibido = LINE.decode('utf-8').split(" ")
        recibido = LINE.decode('UTF-8')
        log('Received from ' + IP_CLIENTE + ':'
            + PUERTO_CLIENTE + recibido, log_path)
        # print(LINE)
        print(recibido)
        metodo = recibido.split(' ')[0]
        if metodo == 'INVITE':
            puerto_rtp = int(recibido.split('\r\n')[8].split(' ')[1])
            ip_rtp = recibido.split('\r\n')[5].split(' ')[1]
            # print(puerto_rtp, ip_rtp)
            self.list_rtp.append(ip_rtp)
            self.list_rtp.append(puerto_rtp)
            v = "v=0\r\n"
            o = 'O=' + username + ' ' + ip + '\r\n'
            s = 's=mysession\r\n'
            t = 't=0\r\n'
            m = 'm=audio ' + str(puerto_audio) + ' RTP\r\n'
            typo = v + o + s + t + m
            longitud = len(typo)
            LINE = 'SIP/2.0 100 Trying\r\n\r\n' \
                   'SIP/2.0 180 Ringing\r\n\r\n' \
                   'SIP/2.0 200 OK\r\n' \
                   'Content-Type: application/sdp\r\nContent-Length: ' \
                   + str(longitud) + '\r\n\r\n' + typo + '\r\n\r\n'
            self.wfile.write(bytes(LINE, 'utf-8'))
            print(self.list_rtp)
            log('Sent to' + IP_CLIENTE + ':' + PUERTO_CLIENTE + LINE, log_path)
        elif metodo == 'BYE':
            self.list_rtp = []
            self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
            log('Sent to' + IP_CLIENTE + ':' + PUERTO_CLIENTE
                + 'SIP/2.0 200 OK\r\n\r\n', log_path)
        elif metodo == 'ACK':
            print(self.list_rtp)
            BIT = secrets.randbelow(1)
            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(version=2, marker=BIT, payload_type=14,
                                  cc=random.randint(3, 15))
            csrc = [random.randint(2, 15),
                    random.randint(2, 15),
                    random.randint(2, 15),
                    random.randint(2, 15),
                    random.randint(2, 15),
                    random.randint(2, 15)]
            RTP_header.setCSRC(csrc)
            audio = simplertp.RtpPayloadMp3(audio_path)
            simplertp.send_rtp_packet(RTP_header, audio,
                                      self.list_rtp[0], self.list_rtp[1])
            os.system('cvlc rtp://@' + self.list_rtp[0] + ':'
                      + str(self.list_rtp[1]) + ' &')
        else:
            trying = recibido.split('\r\n\r\n')[0] == 'SIP/2.0 100 Trying'
            ringing = recibido.split('\r\n\r\n')[1] == 'SIP/2.0 180 Ringing'
            ok = recibido.split('\r\n\r\n')[2].split('\r\n')[0] \
                == 'SIP/2.0 200 OK'
            if trying and ringing and ok:
                print(recibido.split('\r\n'))
                puerto_rtp = int(recibido.split('\r\n')[12].split(' ')[1])
                ip_rtp = recibido.split('\r\n')[9].split(' ')[1]
                BIT = secrets.randbelow(1)
                RTP_header = simplertp.RtpHeader()
                RTP_header.set_header(version=2, marker=BIT, payload_type=14,
                                      cc=random.randint(3, 15))
                csrc = [random.randint(2, 15),
                        random.randint(2, 15),
                        random.randint(2, 15),
                        random.randint(2, 15),
                        random.randint(2, 15),
                        random.randint(2, 15)]
                RTP_header.setCSRC(csrc)
                audio = simplertp.RtpPayloadMp3(audio_path)
                simplertp.send_rtp_packet(RTP_header,
                                          audio, ip_rtp, puerto_rtp)
                os.system('cvlc rtp://@' + ip_rtp + ':'
                          + str(puerto_rtp) + ' &')


if __name__ == "__main__":
    try:
        configuracion = sys.argv[1]
    except IndexError:
        sys.exit("Usage: python3 uaserver.py config")
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    try:
        parser.parse(open(configuracion))
    except FileNotFoundError:
        sys.exit("Usage: python3 uaserver.py config")
    diccionario_server = cHandler.get_tags()
    if diccionario_server['uaserver.ip'] == '':
        ip = '127.0.0.1'
    else:
        ip = diccionario_server['uaserver.ip']
    if diccionario_server['regproxy.ip'] == '':
        ip_proxy = '127.0.0.1'
    else:
        ip_proxy = diccionario_server['regproxy.ip']
    log_path = cHandler.diccionario['log']
    puerto = int(cHandler.diccionario['uaserver.puerto'])
    puerto_proxy = int(cHandler.diccionario['regproxy.puerto'])
    username = cHandler.diccionario['account.username']
    puerto_audio = int(cHandler.diccionario['rtpaudio.puerto'])
    audio_path = cHandler.diccionario['audio']
    serv = socketserver.UDPServer((ip, puerto), EchoHandler)
    log('Starting...', log_path)
    print("Listening...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print('Finalizado servidor')
        log('Finishing.', log_path)
    # print(cHandler.get_tags())
